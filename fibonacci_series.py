#Generator that returns first n numbers in the Fibonacci series
def gen_fib(n = 10):
    _a = 0
    _b = 1
    _i = 0
    while _i < n:
        yield _a
        _a, _b = _b, _a + _b
        _i += 1

#Iterative Fibonachhi series that prints first n numbers in the Fibonacci series
def iter_fib(n = 10):
    _a = 0
    _b = 1
    _i = 0
    while _i < n:
        print(_a)
        _a, _b = _b, _a + _b
        _i += 1

#Recursive Fibonachhi series that prints first n + 1 numbers in the Fibonacci series
def recursive_fib(n = 10):
    return n if n < 2 else recursive_fib(n - 1) + recursive_fib(n - 2)

#Memoized Recursive Fibonachhi series that prints first n + 1 numbers in the Fibonacci series
def memoized_fib(n = 5, cache = {}):
    if n+1 in cache:
        return cache[n+1]
    else:
        cache[n + 1] = n if n < 2 else memoized_fib(n - 1, cache) + memoized_fib(n - 2, cache)
        return cache[n + 1]

#Verbose Memoized Recursive Fibonachhi series that prints first n + 1 numbers in the Fibonacci series
# Displays the cache and the hits to the cache
def memoized_fib(n = 5, cache = {'hits':0}, verbose = False):
    if verbose == True: print(cache)
    if n in cache:
    	cache['hits'] += 1
        return cache[n]
    else:
        cache[n] = n if n < 2 else memoized_fib(n - 1, cache, verbose) + memoized_fib(n - 2, cache, verbose)
        return cache[n]

        
#Decorated Memoized Recursive Fibonachhi series that prints numbers n + 1 digits in the Fibonacci series
#Thanks to http://ujihisa.blogspot.com/2010/11/memoized-recursive-fibonacci-in-python.html
def memoize(f):
    cache = {}
    return lambda *args: cache[args] if args in cache else cache.update({args: f(*args)}) or cache[args]

@memoize
def dec_memo_fib(n = 5):
    return n if n < 2 else dec_memo_fib(n-1) + dec_memo_fib(n-2)


