import os
import time
import fnmatch
from collections import defaultdict

#http://szudzik.com/ElegantPairing.pdf
#This is used to combine size_of_file_in_bytes and last_modified into one number associated uniquely to both
#using a pairing function.
def elegant_pairing_fn(x,y):
    if x == max(x,y):
        return (x*x)+x+y
    else:
        return (y*y)+x

#generates a sequence of file names for the given search pattern starting from the top directory.
def gen_find(filepat, top):
    for path, dirlist, filelist in os.walk(top):
        for name in fnmatch.filter(filelist, filepat):
            yield os.path.join(path,name)

#generates a sequence of dictionary objects with detailed file information.
def gen_stat(filelist):
    for name in filelist:
        dstat = (dict(zip(osstatcolnames,os.stat(name))))
        dstat['filename'] = name
        dstat['unique_id'] = elegant_pairing_fn(dstat['size_of_file_in_bytes'], dstat['last_modified'])
        yield dstat

osstatcolnames=('protection_bits', 'inode_number', 'device', 'number_of_hard_links'
          , 'user_id_of_owner', 'group_id_of_owner', 'size_of_file_in_bytes'
          , 'last_accessed', 'last_modified', 'created')

t0 = time.time()
#jpgfiles = gen_find("*.jpg", "C:\\Users\\Shantanu\\Documents\\Programming\\Python\\Pycasa\\Test")
jpgfiles = gen_find("*.jpg", "C:\\Users\\Shantanu\\Pictures")
#jpgfiles = gen_find("*.jpg", "H:\\DATA")
jpgfilestats=gen_stat(jpgfiles)

#Now group the file names with the same unique_id value
#The algorithm assumes that the combination of file size and last modified date is unique per image file.
# Meaning it is almost impossible to have images that are different and yet have the same size and last modified date.
# This is a safe assumption as long as the files have not been modified using some photo editing software.
filecount = 0
jpgfilegroups = defaultdict(list)
for l in jpgfilestats:
    filecount += 1
    jpgfilegroups[l['unique_id']].append(l['filename'])

#Print groups with more than one file names in it.
#  We do not care to review files that appear only once in our stash.
count = 0
groupcount = 0
for k,v in jpgfilegroups.items():
    groupcount += 1
    if len(v) > 1:
        count += 1
        print(count, k, v)
print ("Total files processed: ", filecount)
print ("Total file groups processed: ", groupcount)
print ("Total time taken: ", time.time() - t0)
