This is a repo for my Python projects. Here I learn, demonstrate and explain some advanced python techniques that come in very handy.

Feel free to review and provide input on any bugs you find or any improvements you have to suggest.
