#Generator that returns first n digits of pi
def gen_digits_of_pi(n=10):
    numer = 355
    denom = 113
    radix = 10
    _i = 0
    while _i < n:
        yield numer/denom
        numer = (numer % denom) * radix if (numer % denom) < denom else denom
        _i += 1
